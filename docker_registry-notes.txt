<------------------------Some problems------------------------------------------------>
1. if you cant login docker :  sudo apt install gnupg2 pass
2. Install apache-utils for htpasswd  
3. htpasswd -Bbn docker-user docker123 >auth/htpasswd
    -B --- Force bcrypt encryption of the password (very secure).
    -b ---Use the password from the command line rather than prompting for it
    -n ---Don't update file; display results on stdout.
<-------------------------------------------------------------------------------------->
<-----------------------------for worker nodes----------------------------------------->
/etc/docker/daemon.json 
{
    "insecure-registries": ["192.168.92.200:5000"]
}

systemctl restart docker
<---------------------------------------------------------------------------------------->
<----------------create kubernetes secret for registry----------------------------------->
kubectl create secret docker-registry mydockercredentials --docker-server 192.168.92.200:5000 --docker-username docker-user --docker-password docker123